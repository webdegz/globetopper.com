<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('COMPANY', 'Globetopper');
define('COPY_RIGHT', 'Copyright © 2015 Globetopper.Com - All right reserved. Powered by Solzsoft.Com');
define('INV_LOGIN', 'Invalid login credentials');
define('REG_REQUIRED', 'Email ID, Username or Password field must not be empty');
define('INV_EMAIL', 'Invalid Email ID');
define('INV_USERNAME', 'Username must be alphanumeric & longer than or equals 5 chars');
define('UUP_USERNAME', 'Username already exist');
define('INV_PASS', 'Your Password Must Contain At Least 8 Characters!');
define('REG_EMAIL_CONF', 'To finish setting up your account, just click the link in the email we sent you.');
define('CONTACT_ADDED', 'Your Contact Successfully Added!');
define('INV_ACT_LINK', 'Invalid Activate link');
define('ACT_ACTIVATED', 'Account Successfully Activated!, <a href="/login">login</a> your account.');
define('PROFILE_UPD', 'Your profile successfully updated!');
define('PASSWORD_UPD', 'Your password successfully updated!');


//Subjects
define('ACT_ACC', 'Active Your Account');

//Paypal
define('PayPalCurrencyCode', 'USD');
define('PayPalApiUsername', 'cspan_api1.globetopper.com');
define('PayPalApiPassword', 'SLCX67C7HXV5AJNC');
define('PayPalApiSignature', 'AFcWxV21C7fd0v3bYYYRCpSSRl31A1q6o1SH8BF4J6vYDSc7keJE8yC3');
define('PayPalMode', 'sandbox');

/* End of file constants.php */
/* Location: ./application/config/constants.php */