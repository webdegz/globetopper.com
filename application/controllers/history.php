<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class history extends MY_Controller {
    
    public $history = NULL;
     
    public function __construct() {
        parent::__construct();   
        
        $this->load->model("m_history");
        $this->history = new m_history();
    }
  
    public function purchase() {    
        $this->data['purchases']=$this->history->get_purchase($this->iduser);
        $this->data['heading']="Purchase History";
        $this->data['heading_slug']="See your all purchase transation history";
        $this->data['title']="Purchase History";
        $this->load->view('history/purchase', $this->data);
    }
    
    public function receipt($idTransaction){
        $this->data['purchase']=$this->history->get_purchase_detail($this->iduser, $idTransaction);
        $this->data['heading']="Purchase Receipt";
        $this->data['heading_slug']="See your all purchase transation history";
        $this->data['title']="Purchase Receipt";
        $this->load->view('history/receipt', $this->data);
    }
}

?>