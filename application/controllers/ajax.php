<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ajax extends CI_Controller {
    
    public $topup = NULL;
    public $data = NULL;
    
    public function __construct() {
        parent::__construct(); 
        
        $this->load->model("m_topup");
        $this->topup = new m_topup();
    }
    
    public function GetProducts(){
        if($this->input->post('phoneNumber')){
            $phoneNumber=$this->input->post('phoneNumber');
            $numberInfo=$this->topup->API('GetOperatorDataByDestinationNumber', array('destinationNumber' => $phoneNumber));                         
            if($numberInfo->records){
                $countryName=trim($numberInfo->records->country);
                $countries=$this->topup->API('GetCountries');
                $countryCode=NULL;
                        
                foreach($countries->records as $country){                    
                    if($country->country_name == $countryName){                        
                        $countryCode=$country->country_code;
                    }
                }
                
                if($countryCode){
                    $Products=$this->topup->API('GetProducts', array('countryCode' => $countryCode));
                    $this->data['Products']=$Products->records;
                    $this->data['countryName']=$countryName;
                    $this->load->view('topup/products', $this->data);
                }else{
                    echo "failed";
                }
            }else{
                echo "failed";
            }
        }
    }    
    
    public function GetAmounts(){
        if($this->input->post('product_id') && $this->input->post('country')){
            $product_id=$this->input->post('product_id');
            $country=$this->input->post('country');            
            $Amounts=$this->topup->API('GetAmountsByProduct', array('productID' => $product_id));                        
            $converstion=$this->topup->currencyAPI($country);
            $newArray=array();
            $x=0;
            foreach($Amounts->records[0]->amounts as $key => $Amount){                
                $newArray[$x]['Amount']=$Amount;
                $newArray[$x]['converstionRate']=$Amount*$converstion['rate'];
                $newArray[$x]['Code']=$converstion['code'];
                $x++;
            }
            $this->data['Amounts']=$newArray;
            $this->load->view('topup/amounts', $this->data);
        }
    }

}

?>