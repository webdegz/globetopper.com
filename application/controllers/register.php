<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class register extends CI_Controller {
    
    public $user = NULL;
    public $data = NULL;
     
    public function __construct() {
        parent::__construct(); 
        
        $this->load->model("m_user");
        $this->user = new m_user();
        
        if($this->user->is_user_login()){
            redirect(base_url() . "dashboard");
        }
    }
  
    public function index() {  
        $this->data['confirmation']=0;
        if($this->input->post('register')){
            if($this->user->register($this->input->post('username'), $this->input->post('email'), $this->input->post('password'))){                               
                $message_array = array(
                    '%username%' => $this->input->post('username'),
                    '%activate_url%' => base_url() . 'activate/?username='.$this->input->post('username').'&email='.$this->input->post('email')
                );                
                $this->user->SendEmail($this->input->post('email'), 'user_signup_activation', $message_array);
                
                if($this->user->login($this->input->post('username'), $this->input->post('password'))){
                    if($this->input->get('action') == 'redirect'){
                        if($this->input->get('controler') && $this->input->get('phone_number') && $this->input->get('product') && $this->input->get('amount')){
                            redirect(base_url().$this->input->get('controler')."/?phone_number=".$this->input->get('phone_number')."&product=".$this->input->get('product')."&amount=".$this->input->get('amount'));
                        }
                    }else{
                        redirect(base_url()."dashboard/");
                    }
                }
                $this->data['confirmation']=1;
            }
        }
        $this->data['title']="Create New Account";
        $this->load->view('register', $this->data);
    }

}

?>