<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class payment extends MY_Controller {
    
    public $payment = NULL;
     
    public function __construct() {
        parent::__construct();   
        
        $this->load->model("m_payment");
        $this->payment = new m_payment();
    }
  
    public function index(){        
        if($this->input->post('do_payment')){
            $this->payment->do_express_checkout($this->input->post('countryName'), $this->input->post('amount'), $this->input->post('phone_number'), $this->input->post('product'));
        }
        
        if($this->input->get('token') && $this->input->get('PayerID')){
            $this->payment->do_payment($this->input->get('token'),$this->input->get('PayerID'));
        }
    }        
        
}

?>