<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class profile extends MY_Controller {        
     
    public function __construct() {
        parent::__construct();           
    }      
    
    public function index() {        
                
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('firstname', 'Firstname is required', 'required');        
        $this->form_validation->set_rules('city', 'City is required', 'required');
        $this->form_validation->set_rules('state', 'State is required', 'required');
        $this->form_validation->set_rules('country', 'Select your country', 'required');
        
        if ($this->form_validation->run() == TRUE){                         
            $this->user->update_profile($this->input->post('firstname'), $this->input->post('lastname'), $this->input->post('gender'), $this->input->post('dob'), $this->input->post('city'), $this->input->post('state'), $this->input->post('country'), $this->input->post('instagram'), $this->input->post('twitter'), $this->input->post('linkedin'), $this->iduser);
            $this->session->set_userdata(array('success' => PROFILE_UPD));
            redirect(base_url().'profile/');
        }
        
        $this->data['countries']=$this->user->API('GetCountries');        
        $this->data['heading']="My Profile";
        $this->data['heading_slug']="Edit your profile here.";
        $this->data['title']="My Profile";
        $this->load->view('profile', $this->data);
    }
    
    public function password(){
        
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', 'new password', 'required|matches[passconf]');
        $this->form_validation->set_rules('passconf', 'password confirmation', 'required');
        
        if ($this->form_validation->run() == TRUE){                         
            $this->user->update_password($this->input->post('password'), $this->iduser);
            $this->session->set_userdata(array('success' => PASSWORD_UPD));
            redirect(base_url().'logout/');
        }
        
        $this->data['heading']="Change Password";
        $this->data['heading_slug']="You can change your password here.";
        $this->data['title']="Change Password";
        $this->load->view('password', $this->data);
    }

}

?>