<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class logout extends MY_Controller {
        
    public function __construct() {
        parent::__construct();                 
    }
  
    public function index() {    
        
        $this->load->library('facebook');
        // Logs off session from website
        $this->facebook->destroySession();
        // Make sure you destory website session as well.
        session_destroy();
        $array_items = array('iduser' => '');
        $this->session->unset_userdata($array_items);
        redirect(base_url());
    }

}

?>