<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class dashboard extends MY_Controller {        
    
    public $history = NULL;
    public $contacts = NULL;
    
    public function __construct() {
        parent::__construct(); 
        
        $this->load->model("m_history");
        $this->history = new m_history();
        
        $this->load->model("m_contacts");
        $this->contacts = new m_contacts();
    }
  
    public function index() {  
        $this->data['purchases']=$this->history->get_purchase($this->iduser, array(0,5));
        $this->data['contacts']=$this->contacts->get_contacts($this->iduser, array(0,5));
        
        if($savedContacts=$this->contacts->get_contacts($this->iduser)){
            $this->data['savedContacts']=count($savedContacts);
        }else{
            $this->data['savedContacts']=0;
        }       
        
        if($purchasesMade=$this->history->get_purchase($this->iduser)){
            $this->data['purchasesMade']=count($purchasesMade);
        }else{
            $this->data['purchasesMade']=0;
        }
        
                
                
        $this->data['heading']="Welcome to ".COMPANY;
        $this->data['heading_slug']="Send a top-up and we’ll list your recent transactions when you login again.";
        $this->data['title']="Account Overview";
        $this->load->view('dashboard', $this->data);
    }

}

?>