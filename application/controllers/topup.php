<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class topup extends CI_Controller {
    
    public $user = NULL;
    public $data = NULL;
    public $topup = NULL;
     
    public function __construct() {
        parent::__construct();   
        
        $this->load->model("m_topup");
        $this->topup = new m_topup();
        
        $this->load->model("m_user");
        $this->user = new m_user();
    }
  
    public function index(){
        if($iduser=$this->user->is_user_login()){
            $iduser=$this->session->userdata('iduser');
            $this->data['user_detail']=$this->user->get_user_detail($iduser);
        }
        $this->data['heading']="Send Top-up";
        $this->data['heading_slug']="Send Top-up to any number in the world.";
        $this->data['title']="Send Top-up";                  
        $this->load->view('topup/index', $this->data);
        
    }            

}

?>