<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class login extends CI_Controller {
    
    public $user = NULL;
    public $data = NULL;
     
    public function __construct() {
        parent::__construct(); 
        
        $this->load->model("m_user");
        $this->user = new m_user();
        
        $this->load->helper('url');
        $this->load->library('facebook'); // Automatically picks appId and secret from config
        
        if($this->user->is_user_login()){
            if($this->input->get('action') == 'redirect'){
                if($this->input->get('controler') && $this->input->get('phone_number') && $this->input->get('product') && $this->input->get('amount')){
                    redirect(base_url().$this->input->get('controler')."/?phone_number=".$this->input->get('phone_number')."&product=".$this->input->get('product')."&amount=".$this->input->get('amount'));
                }
            }else{
                redirect(base_url()."dashboard/");
            }            
            exit;
        }
    }
  
    public function index() {        
        
        $fb_user = $this->facebook->getUser();        
        
        if ($fb_user) {                        
            try {
                $fb_user_profile=$this->facebook->api('/me');                  
                if($this->user->fb_login_register($fb_user, $fb_user_profile['name'])){
                    if($this->input->get('action') == 'redirect'){
                        if($this->input->get('controler') && $this->input->get('phone_number') && $this->input->get('product') && $this->input->get('amount')){
                            redirect(base_url().$this->input->get('controler')."/?phone_number=".$this->input->get('phone_number')."&product=".$this->input->get('product')."&amount=".$this->input->get('amount'));
                        }
                    }else{
                        redirect(base_url()."dashboard/");
                    }
                    exit;
                }else{
                    $this->session->set_userdata(array('error' => INV_LOGIN));
                }
            } catch (FacebookApiException $e) {
                $fb_user = null;
            }
        }else{
            $this->data['login_url'] = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('login'), 
                'scope' => array("email", "public_profile") // permissions here
            ));
        }            
        
        if($this->input->post('login')){
            if($this->user->login($this->input->post('username'), $this->input->post('password'))){
                if($this->input->get('action') == 'redirect'){
                    if($this->input->get('controler') && $this->input->get('phone_number') && $this->input->get('product') && $this->input->get('amount')){
                        redirect(base_url().$this->input->get('controler')."/?phone_number=".$this->input->get('phone_number')."&product=".$this->input->get('product')."&amount=".$this->input->get('amount'));
                    }
                }else{
                    redirect(base_url()."dashboard/");
                }
                
            }else{
                $this->session->set_userdata(array('error' => INV_LOGIN));
            }
        }
        $this->data['title']="Login";
        $this->load->view('login', $this->data);
    }

}

?>