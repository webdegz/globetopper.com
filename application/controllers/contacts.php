<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class contacts extends MY_Controller {
    
    public $contacts = NULL;
     
    public function __construct() {
        parent::__construct();   
        
        $this->load->model("m_contacts");
        $this->contacts = new m_contacts();
    }
  
    public function saved_numbers() {
        
        $this->data['contacts']=$this->contacts->get_contacts($this->iduser);
        $this->data['heading']="Your ".COMPANY." Contacts";
        $this->data['heading_slug']="Save your phone numbers here to make it even easier to top up.";
        $this->data['title']="Saved Contacts";
        $this->load->view('contacts/saved_numbers', $this->data);
    }
    
    public function add_contact() {                
        
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('contact_name', 'Contact Name', 'required');
        $this->form_validation->set_rules('contact_phone', 'Contact Phone Number', 'required');
        
        if ($this->form_validation->run() == TRUE){                         
            $this->contacts->add_contact($this->input->post('contact_name'), $this->input->post('contact_phone'), $this->iduser);
            $this->session->set_userdata(array('success' => CONTACT_ADDED));
        }     
        
        $this->data['heading']="Add Your Contact";
        $this->data['heading_slug']="Save your phone numbers here to make it even easier to top up.";
        $this->data['title']="Add Your Contact";
        $this->load->view('contacts/add_contact', $this->data);
    }

}

?>