<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class privacy_policy extends CI_Controller {
    
    public $user = NULL;
    public $data = NULL;
     
    public function __construct() {
        parent::__construct();   
        $this->load->model("m_user");
        $this->user = new m_user();
    }
  
    public function index(){  
        if($iduser=$this->user->is_user_login()){
            $iduser=$this->session->userdata('iduser');
            $this->data['user_detail']=$this->user->get_user_detail($iduser);
        }
        $this->data['heading']="Privacy Policy";
        $this->data['heading_slug']="Privacy Policy";
        $this->data['title']="Privacy Policy";                  
        $this->load->view('pages/privacy_policy', $this->data);
        
    }            

}

?>