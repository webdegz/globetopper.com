<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class promotion extends CI_Controller {
    
    public $user = NULL;
    public $data = NULL;
     
    public function __construct() {
        parent::__construct();          
        
        $this->load->model("m_user");
        $this->user = new m_user();
    }
  
    public function index(){
        if($iduser=$this->user->is_user_login()){
            $iduser=$this->session->userdata('iduser');
            $this->data['user_detail']=$this->user->get_user_detail($iduser);
        }
        $this->data['heading']="Promotion";
        $this->data['heading_slug']="Promotion";
        $this->data['title']="Promotion";                  
        $this->load->view('pages/promotion', $this->data);
        
    }            

}

?>