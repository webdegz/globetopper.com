<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class activate extends CI_Controller {
    
    public $user = NULL;
    public $data = NULL;
     
    public function __construct() {
        parent::__construct(); 
        
        $this->load->model("m_user");
        $this->user = new m_user();                
        
        if($this->user->is_user_login()){
            redirect(base_url() . "dashboard");
            exit;
        }
    }
  
    public function index() {        
        if(!$this->input->get('username') || !$this->input->get('email')){
            $this->session->set_userdata(array('error' => INV_ACT_LINK));
        }else{
            if($this->user->activate_account($this->input->get('username'),$this->input->get('email'))){
                $this->session->set_userdata(array('success' => ACT_ACTIVATED));
            }else{
                $this->session->set_userdata(array('error' => INV_ACT_LINK));
            }
        }        
        $this->data['title']="Activate Your Account";
        $this->load->view('activate', $this->data);
    }

}

?>