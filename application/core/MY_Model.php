<?php
/**
 * Name : MY_Model
 * Description: Extending the Codeigniter CI_Model functionality into My_Model for Basic CRUD
 * Purpose : In future Updates in CodeIgniter we can adopt this by just simple change of this blueprint
 * CI Version : 2.2.0
 * Dated : 27 Aug 2014 1:00:00 PM 
 * Version : 1.0  for this blueprint
 * @author Administrator
 */
class MY_Model extends CI_Model {

    /**
     * @name __contruct 
     * @decription : Overrides the parent contructor Default call when instantiating the class
     * @author Muazzam Ali 
     * @return class object;
     */
    public function __construct() {
        parent::__construct(); // parent constructor 		
    }
	
    public function FormateDate($date, $time=false){
        if($time==TRUE){
                $value=date('Y-m-d H:i:s', strtotime($date));
        }else{
                $value=date('F d, Y', strtotime($date));
        }

        return $value;
    }

    public function message_box($value){
            $this->session->unset_userdata('message');
            return '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>Done!</strong> '.$value.'</div>';				
    }

    public function error_box($value){
            $this->session->unset_userdata('error');
            return '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><strong>Sorry!</strong> '.$value.'</div>';				
    }
    
    public function SendEmail($ToEmail, $MessageSlug, $MessageArray=NULL, $cc=NULL,$bcc=NULL, $FromName=COMPANY, $FromEmail="info@globetopper.com"){
        $this->email->from($FromEmail, $FromName);
        $this->email->to($ToEmail);             
        
        if($MessageArray){
            $GetMessage=$this->get_message($MessageSlug, $MessageArray);
        }else{
            $GetMessage=$this->get_message($MessageSlug);
        }                
        
        $this->email->subject($GetMessage['Subject']);
        $this->email->message($GetMessage['Template']);	      
        
        $this->email->set_mailtype("html");
        $this->email->send();

        return $this->email->print_debugger();
    }
    
    public function get_message($slug, $Array=NULL){
        $query=$this->db->query("SELECT fr_message_templates.idMessage, fr_message_templates.Message, fr_message_templates.Subject FROM fr_message_templates WHERE fr_message_templates.slug = '{$slug}'");
        if($query->num_rows() > 0){
            $Result=$query->result();
            $Template=$Result[0]->Message;
            $Subject=$Result[0]->Subject;
            if($Array){
                foreach($Array as $Key => $Arr){
                    $Template=str_replace($Key, $Arr, $Template);
                }
            }
            return array('Template' => $Template, 'Subject' => $Subject);
        }else{
            return FALSE;
        }
    }
    
    public function API($action, $arg=NULL, $json=FALSE){
        $username="globetopper";
        $password="dd3ecce5612e5e3e69699b34dd419837"; 
        $query=NULL;
        
        if($arg){
            $query="&".http_build_query($arg);            
        }
        
        $URL='https://www.globetopper.com/index.php?r=APITopUp/Run&u='.$username.'&pw='.$password.'&action='.$action.$query;
        
        if($json == FALSE){
            $data=json_decode(file_get_contents($URL));
        }else{
            $data=file_get_contents($URL);
        }
        return $data;
    }
    
    public function currencyAPI($country){
        $query=$this->db->query("SELECT countries.currency_code FROM countries WHERE countries.`name` = '{$country}'");
        if($query->num_rows() > 0){
            $result=$query->result();
            $currency_code=$result[0]->currency_code;
            $response=file_get_contents('http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22USD'.$currency_code.'%22)&env=store://datatables.org/alltableswithkeys');
            $xml = new SimpleXMLElement($response);
            $array['rate']=$xml->results->rate->Rate;
            $array['code']=$currency_code;
            return $array;
        }else{
            return false;
        }
        
    }
}
