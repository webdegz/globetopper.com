<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MY_Controller
 *
 * @author Administrator
 */
class MY_Controller extends CI_Controller { 
	
    public $user = NULL;
    public $iduser = NULL;
    public $data = NULL;
	
    public function __construct() {
        parent::__construct();
        
        $this->load->model("m_user");
        $this->user = new m_user();

        if($iduser=$this->user->is_user_login()){    
            $iduser=$this->session->userdata('iduser');
            $this->data['user_detail']=$this->user->get_user_detail($iduser);
            $this->iduser=$iduser;
        }else{
            redirect(base_url() . "login");
        }
		
    }
}
