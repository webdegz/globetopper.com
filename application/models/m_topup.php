<?php
class m_topup extends MY_Model {
    
    public function __construct() {
        parent::__construct(); // parent constructor				
    }	 
    
    public function getCountries(){
        $query=$this->db->query("SELECT countries.countries_id, countries.countries_name, countries.countries_iso_code_2, countries.countries_iso_code_3, countries.address_format_id FROM countries ORDER BY countries.countries_name ASC");        
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return FALSE;
        }
    }
}
