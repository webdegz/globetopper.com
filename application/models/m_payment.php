<?php
class m_payment extends MY_Model {        
    
    public function __construct() {
        parent::__construct(); // parent constructor				
    }    
    
    public function do_express_checkout($countryName=NULL,$amount=NULL,$phone_number=NULL,$product=NULL){
        
        if($countryName && $amount && $phone_number && $product){
            
            $PayPalReturnURL = base_url()."payment/";
            $PayPalCancelURL = base_url()."cancel/";
            
            $ItemName = "Top-up (".$countryName.")"; //Item Name
            $ItemPrice = $amount; //Item Price
            $ItemNumber = $phone_number; //Item Number
            $ItemQty = 1; // Item Quantity
            $ItemTotalPrice = ($ItemPrice*$ItemQty); //(Item Price x Quantity = Total) Get total amount of product; 

            $padata = 	'&CURRENCYCODE='.urlencode(PayPalCurrencyCode).
                            '&PAYMENTACTION=Sale'.
                            '&ALLOWNOTE=1'.
                            '&PAYMENTREQUEST_0_CURRENCYCODE='.urlencode(PayPalCurrencyCode).
                            '&PAYMENTREQUEST_0_AMT='.urlencode($ItemTotalPrice).
                            '&PAYMENTREQUEST_0_ITEMAMT='.urlencode($ItemTotalPrice). 
                            '&L_PAYMENTREQUEST_0_QTY0='. urlencode($ItemQty).
                            '&L_PAYMENTREQUEST_0_AMT0='.urlencode($ItemPrice).
                            '&L_PAYMENTREQUEST_0_NAME0='.urlencode($ItemName).
                            '&L_PAYMENTREQUEST_0_NUMBER0='.urlencode($ItemNumber).
                            '&AMT='.urlencode($ItemTotalPrice).                            				
                            '&RETURNURL='.urlencode($PayPalReturnURL ).
                            '&CANCELURL='.urlencode($PayPalCancelURL);
            
            $httpParsedResponseAr = $this->PPHttpPost('SetExpressCheckout', $padata, PayPalApiUsername, PayPalApiPassword, PayPalApiSignature, PayPalMode);		
            
            if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])){		                                
                
                $sessionArray=array('itemprice' => $ItemPrice, 'totalamount' => $ItemTotalPrice, 'itemName' => $ItemName, 'itemNo' => $ItemNumber, 'itemQTY' => $ItemQty, 'product' => $product, 'countryName' => $countryName);
                $this->session->set_userdata(array('paymentSession' => $sessionArray));
                
                if(PayPalMode=='sandbox'){                
                    $paypalmode = '.sandbox';
                }
                else{                
                    $paypalmode = "";
                }
                //Redirect user to PayPal store with Token received.
                $paypalurl ='https://www'.$paypalmode.'.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token='.$httpParsedResponseAr["TOKEN"].'';
                header('Location: '.$paypalurl);
            }else{                
                $this->session->set_userdata(array('error' => urldecode($httpParsedResponseAr["L_LONGMESSAGE0"])));
            }
            
        }else{
            $this->session->set_userdata(array('error' => "Invalid Payment"));
        }
    }
    
    public function do_payment($token,$playerid){
        
        if(!$this->session->userdata('paymentSession')){
            echo "Invalid URL";
            exit;
        }
        $sessionArray=$this->session->userdata('paymentSession');        
        
        //get session variables
	$ItemPrice=$sessionArray['itemprice'];
	$ItemTotalPrice=$sessionArray['totalamount'];
	$ItemName=$sessionArray['itemName'];	
	$ItemNumber=$sessionArray['itemNo'];	
	$ItemQTY=$sessionArray['itemQTY'];
        $product = $sessionArray['product'];
        $countryName = $sessionArray['countryName'];
        
        $sessionArray=$this->session->unset_userdata('paymentSession');
        
        
        $padata = 	'&TOKEN='.urlencode($token).
						'&PAYERID='.urlencode($playerid).
						'&PAYMENTACTION='.urlencode("SALE").
						'&AMT='.urlencode($ItemTotalPrice).
						'&CURRENCYCODE='.urlencode(PayPalCurrencyCode);
        $httpParsedResponseAr = $this->PPHttpPost('DoExpressCheckoutPayment', $padata, PayPalApiUsername, PayPalApiPassword, PayPalApiSignature, PayPalMode);
        
        if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])){
            
            if('Completed' == $httpParsedResponseAr["PAYMENTSTATUS"]){
                
                $TRANSACTIONID=urlencode($httpParsedResponseAr["TRANSACTIONID"]);
                $nvpStr = "&TRANSACTIONID=".$TRANSACTIONID;
                $httpParsedResponseAr = $this->PPHttpPost('GetTransactionDetails', $nvpStr, PayPalApiUsername, PayPalApiPassword, PayPalApiSignature, PayPalMode);
                
                if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
                                    
                    //Insert Data into table
                    $data = array(
                        'idTransaction' => uniqid(),
                        'iduser' => $this->session->userdata('iduser'),
                        'TRANSACTIONID' => $TRANSACTIONID,
                        'PAYMENTSTATUS' => 'Completed',
                        'FIRSTNAME' => $httpParsedResponseAr["FIRSTNAME"],
                        'LASTNAME' => $httpParsedResponseAr["LASTNAME"],
                        'EMAIL' => $httpParsedResponseAr["EMAIL"],
                        'phoneNumbner' => $ItemNumber,
                        'country' => $countryName,
                        'product' => $product,
                        'amount' => $ItemTotalPrice,
                        'date' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('fr_transactions', $data); 

                    $Topup=$this->API('SendTopUp', array('phone' => $ItemNumber, 'productID' => $product, 'amount' => $ItemTotalPrice)); 
                    echo "<pre>";
                    print_r($Topup);
                    echo "</pre>";
                    
                    echo "Thankyou Page";
                }
            }
                        
        }else{
            $this->session->set_userdata(array('error' => urldecode($httpParsedResponseAr["L_LONGMESSAGE0"])));            
        }
    }
        
    public function PPHttpPost($methodName_, $nvpStr_, $PayPalApiUsername, $PayPalApiPassword, $PayPalApiSignature, $PayPalMode) {

            // Set up your API credentials, PayPal end point, and API version.

            $API_UserName = urlencode($PayPalApiUsername);

            $API_Password = urlencode($PayPalApiPassword);

            $API_Signature = urlencode($PayPalApiSignature);



            if($PayPalMode=='sandbox')

            {

                    $paypalmode 	=	'.sandbox';

            }

            else

            {

                    $paypalmode 	=	'';

            }



            $API_Endpoint = "https://api-3t".$paypalmode.".paypal.com/nvp";

            $version = urlencode('76.0');



            // Set the curl parameters.

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $API_Endpoint);

            curl_setopt($ch, CURLOPT_VERBOSE, 1);



            // Turn off the server and peer verification (TrustManager Concept).

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);



            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            curl_setopt($ch, CURLOPT_POST, 1);



            // Set the API operation, version, and API signature in the request.

            $nvpreq = "METHOD=$methodName_&VERSION=$version&PWD=$API_Password&USER=$API_UserName&SIGNATURE=$API_Signature$nvpStr_";



            // Set the request as a POST FIELD for curl.

            curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);



            // Get response from the server.

            $httpResponse = curl_exec($ch);



            if(!$httpResponse) {

                    exit("$methodName_ failed: ".curl_error($ch).'('.curl_errno($ch).')');

            }



            // Extract the response details.

            $httpResponseAr = explode("&", $httpResponse);



            $httpParsedResponseAr = array();

            foreach ($httpResponseAr as $i => $value) {

                    $tmpAr = explode("=", $value);

                    if(sizeof($tmpAr) > 1) {

                            $httpParsedResponseAr[$tmpAr[0]] = $tmpAr[1];

                    }

            }



            if((0 == sizeof($httpParsedResponseAr)) || !array_key_exists('ACK', $httpParsedResponseAr)) {

                    exit("Invalid HTTP Response for POST request($nvpreq) to $API_Endpoint.");

            }
		
            return $httpParsedResponseAr;

	}
}
