<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Name: M_User Model for User 
 * Description: M_User contains all CRUD functionality to manipulate with database 
 * @author Muazzam Ali
 */
class m_user extends MY_Model {
    public function __construct() {
        parent::__construct(); // parent constructor
				
    }
	
    /**
     * Function to check is User is loged in or not
     * Return TRUE / FALSE
    */
    public function is_user_login() {		
        if($iduser=$this->session->userdata('iduser')){
            $Query=$this->db->query("SELECT fr_users.iduser FROM fr_users WHERE fr_users.iduser = '{$iduser}' AND fr_users.`isactive` = 1 AND fr_users.`status` = 'approved'");			
            if($Query->num_rows() == 1){
                return $iduser;
            }else{
                $array_items = array('iduser' => '');
                $this->session->unset_userdata($array_items);
                return FALSE;
            }
        }else{
            return FALSE;
        }	
    }
    
    public function register($username, $email, $password){
        $validate = 1;
        if(empty($username) || empty($password) || empty($email)){            
            $error_array[]=REG_REQUIRED;
            $validate=0;
        }else{
            if(!preg_match('/^[a-zA-Z0-9]{5,}$/', $username)) {
                $error_array[]=INV_USERNAME;
                $validate=0;
            }else{
                if($this->check_username($username)){
                    $error_array[]=UUP_USERNAME;
                    $validate=0;
                }
            }
            
            if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                $error_array[]=INV_EMAIL;
                $validate=0;
            }
            
            if (strlen($password) <= '8') {                
                $error_array[]=INV_PASS;
                $validate=0;
            }
        }
        
        if($validate == 1){
            $user_data = array(
                'iduser' => uniqid(),  
                'email' => $email,
                'password' => md5($password),
                'isactive' => 1,
                'status' => 'approved',
                'username' => $username,
                'is_fb' => 0,
                'confirmation' => 0,
            );
            $this->db->insert('fr_users', $user_data);
            return TRUE;
        }else{
            $this->session->set_userdata(array('required' => $error_array));
            return FALSE;
        }
    }
    
    public function login($username, $password){
        if(empty($username) || empty($password)){
            return FALSE;
        }else{
            $Query=$this->db->query("SELECT fr_users.iduser, fr_users.username FROM fr_users WHERE fr_users.username = '{$username}' AND fr_users.`password` = '".md5($password)."' AND fr_users.`isactive` = 1 AND fr_users.`status` = 'approved'");
            if($Query->num_rows() == 1){
                $Obj = $Query->result();
                $array_items = array('iduser' => $Obj[0]->iduser, 'username' => $Obj[0]->username);
                $this->session->set_userdata($array_items);                
                return TRUE;
            }else{
                return FALSE;
            }
        }
    }
    
    public function get_user_detail($iduser=NULL){
        if($iduser){
            $query=$this->db->query("SELECT fr_users.instagram, fr_users.twitter, fr_users.linkedin, fr_users.iduser, fr_users.username, fr_users.email, fr_users.`password`, fr_users.firstname, fr_users.lastname, fr_users.gender, fr_users.dob, fr_users.city, fr_users.state, fr_users.country, fr_users.isactive, fr_users.`status` FROM fr_users WHERE fr_users.iduser = '".$iduser."'");
            if($query->num_rows() == 1){
                $result = $query->result();
                return $result[0];
            }else{
                return FALSE;
            }
        }else{
            return FALSE;
        }
    }
    
    public function check_username($username){
        $query=$this->db->query("SELECT iduser FROM fr_users WHERE username='".$username."'");
        if($query->num_rows() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function fb_login_register($username, $fullname){
        if($username){
            if($this->check_username($username)){
                $Query=$this->db->query("SELECT fr_users.iduser, fr_users.email FROM fr_users WHERE fr_users.username = '{$username}' AND fr_users.`isactive` = 1 AND fr_users.`status` = 'approved'");
                if($Query->num_rows() == 1){
                    $Obj = $Query->result();
                }
                $array_items = array('iduser' => $Obj[0]->iduser, 'username' => $Obj[0]->username);
                $this->session->set_userdata($array_items);                
                return TRUE;
            }else{
                $iduser=uniqid();
                $user_data = array(
                    'iduser' => uniqid(),                            
                    'isactive' => 1,
                    'status' => 'approved',
                    'username' => $username,
                    'is_fb' => 1,
                    'firstname' => $fullname,
                    'confirmation' => 1,
                );
                $this->db->insert('fr_users', $user_data);              
                $array_items = array('iduser' => $iduser, 'username' => $username);
                $this->session->set_userdata($array_items);
                return TRUE;
            }
        }else{
            return FALSE;
        }
    }
    
    public function activate_account($username, $email){
        $query=$this->db->query("SELECT fr_users.iduser FROM fr_users WHERE fr_users.username = '{$username}' AND fr_users.email = '{$email}' AND fr_users.`confirmation` = 0 AND fr_users.`isactive` = 1 AND fr_users.`status` = 'approved'");
        if($query->num_rows() == 1){
            $result=$query->result();
            $iduser=$result[0]->iduser;
            
            $data = array(
               'confirmation' => 1
            );

            $this->db->where('iduser', $iduser);
            $this->db->update('fr_users', $data); 
            return TRUE;
        }else{
            return FALSE;
        }
    }
    
    public function update_profile($firstname, $lastname, $gender, $dob, $city, $state, $country, $instagram, $twitter, $linkedin, $iduser){
        $user_data = array(
            'firstname' => $firstname,                            
            'lastname' => $lastname,
            'gender' => $gender,
            'dob' => $dob,
            'city' => $city,
            'state' => $state,
            'country' => $country,
            'instagram' => $instagram,
            'twitter' => $twitter,
            'linkedin' => $linkedin,
        );
        $this->db->where('iduser', $iduser);
        $this->db->update('fr_users', $user_data);
    }
    
    public function update_password($password, $iduser){
        $user_data = array(
            'password' => md5($password)
        );
        $this->db->where('iduser', $iduser);
        $this->db->update('fr_users', $user_data);
    }
}
