<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Name: M_User Model for User 
 * Description: M_User contains all CRUD functionality to manipulate with database 
 * @author Muazzam Ali
 */
class m_history extends MY_Model {
    public function __construct() {
        parent::__construct(); // parent constructor
				
    }	 
        
    public function get_purchase($iduser, $limit=array()){
        
        $limitCondition=NULL;
        
        if(count($limit) > 0){            
            $limitCondition=" LIMIT ".$limit['0'].",".$limit['1'];
        }
        $query=$this->db->query("SELECT fr_transactions.idTransaction, fr_transactions.iduser, fr_transactions.TRANSACTIONID, fr_transactions.PAYMENTSTATUS, fr_transactions.FIRSTNAME, fr_transactions.LASTNAME, fr_transactions.EMAIL, fr_transactions.phoneNumbner, fr_transactions.country, fr_transactions.product, fr_transactions.amount, fr_transactions.date FROM fr_transactions WHERE fr_transactions.iduser = '{$iduser}' ORDER BY fr_transactions.date DESC".$limitCondition);
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }
    
    public function get_purchase_detail($iduser, $idTransaction){
        $query=$this->db->query("SELECT fr_transactions.idTransaction, fr_transactions.iduser, fr_transactions.TRANSACTIONID, fr_transactions.PAYMENTSTATUS, fr_transactions.FIRSTNAME, fr_transactions.LASTNAME, fr_transactions.EMAIL, fr_transactions.phoneNumbner, fr_transactions.country, fr_transactions.product, fr_transactions.amount, fr_transactions.date FROM fr_transactions WHERE fr_transactions.idTransaction = '{$idTransaction}' AND fr_transactions.iduser = '{$iduser}'");
        if($query->num_rows() > 0){
            $result=$query->result();
            return $result[0];
        }else{
            return false;
        }
    }
}
