<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Name: M_User Model for User 
 * Description: M_User contains all CRUD functionality to manipulate with database 
 * @author Muazzam Ali
 */
class m_contacts extends MY_Model {
    public function __construct() {
        parent::__construct(); // parent constructor
				
    }	 
    
    public function add_contact($contact_name, $contact_phone, $iduser){
        $data = array(
            'idcontact' => uniqid(),
            'iduser' => $iduser,
            'contact_name' => $contact_name,
            'contact_phone' => $contact_phone,
            'date' => date('Y-m-d H:i:s')
        );

        $this->db->insert('fr_contacts', $data); 
    }
    
    public function get_contacts($iduser, $limit=array()){
        $limitCondition=NULL;
        
        if(count($limit) > 0){            
            $limitCondition=" LIMIT ".$limit['0'].",".$limit['1'];
        }
        
        $query=$this->db->query("SELECT fr_contacts.idcontact, fr_contacts.iduser, fr_contacts.contact_name, fr_contacts.contact_phone, fr_contacts.date FROM fr_contacts WHERE fr_contacts.iduser = '{$iduser}' ORDER BY fr_contacts.date DESC".$limitCondition);
        if($query->num_rows() > 0){
            return $query->result();
        }else{
            return false;
        }
    }
}
