<?php $this->load->view('header'); ?>
<!-- WELCOME MESSAGE STARTS
========================================================================= -->
<div class="clear"></div>
<div class="container padding-box" id="about-us">
  <div class="row header">
    <article class="col-xs-12 textbox text-center">
      <h2 class="black">Our Commitment to Offer More…</h2>
   <p>Newly formed in 2015 by a longtime Telecom veteran, GlobeTopper offers a wide and ever growing range of International Mobile TopUp products. With service to 500 carriers in 130 countries, GlobeTopper has one of the largest international footprints in the industry. Taking advantage of its extensive regional provider network helps keep costs low and enables us to offer the maximum number of minutes on your TopUps along with extremely aggressive pricing for distributors. </p>    </article>
    <aside class="col-sm-3 col-xs-12">
        <div class="text-center servicesbox">
          <div>
            <div class="icon-box list-inline"><span class="fa fa-globe"></span></div>
            <h4>More Carriers.</h4>
            <div class="separator"><img src="assets/images/circle.png" alt="" /></div>
            <p>Providing service to 500 carriers in 130 countries, GlobeTopper has one of the largest international footprints in the industry.</p>
          </div>
        </div>
      </aside>
      <aside class="col-sm-3 col-xs-12">
        <div class="text-center servicesbox">
          <div>
            <div class="icon-box list-inline"><span class="fa fa-tasks"></span></div>
            <h4>More Options.</h4>
            <div class="separator"><img src="assets/images/circle.png" alt="" /></div>
   <p>Because of our relationships with many regional suppliers, GlobeTopper offers  a wide variety of Products and Services beyond just Airtime.  </p>

          </div>
        </div>
      </aside>
      <aside class="col-sm-3 col-xs-12">
        <div class="text-center servicesbox">
          <div>
            <div class="icon-box list-inline"><span class="fa fa-briefcase"></span></div>
            <h4>More Value.</h4>
                    <div class="separator"><img src="assets/images/circle.png" alt="" /></div>
<p>We believe that real value comes from putting customers first . Our unique model allows for more credits for your TopUp recipient and more value for your money. </p>          </div>
        </div>
      </aside>
      <aside class="col-sm-3 col-xs-12">
        <div class="text-center servicesbox">
          <div>
            <div class="icon-box list-inline"><span class="fa fa-heart"></span></div>
            <h4>More Caring.</h4>
            <div class="separator"><img src="assets/images/circle.png" alt="" /></div>
            <p>A portion of all proceeds will be directed to The Thirst Project (<a href="https://www.thirstproject.org/">thirstproject.org </a>) the World's Leading Youth Water Activism Organization.  Clean water should be a right for all global citizens.
</p>
          </div>
        </div>
      </aside>
  </div>
  <!--container--> 
</div>


<!-- TESTIMONIAL STARTS
========================================================================= -->
<div class="testi-c padding-box" id="testimonial">
  <div class="container">
    <div class="row header">
      <article class="col-xs-12 textbox text-center">   <h2 class="white">What our users say about us.</h2>
        <div class="contents testimonial-carousel">
          <div>
            <p class="white"><i>My mother got more minutes through GlobeTopper than through other services. Thank you. </i></p>
            <p class="white"><span class="author-name"><b>Jay</b></span><br/>
            </p>
          </div>
          <div>
          
          
            <p class="white"><i>Fast and quite easy to use. Will definitely use again.</i></p>
            <p class="white"><span class="author-name"><b>Leonard V.</b></span><br/>
            </p>
          </div>
          <div>
           
          
            <p class="white"><i>Your offerings to India were very attractive for us. I think we found a new provider!</i></p>
            <p class="white"><span class="author-name"><b>Amala</b></span><br/>
              </p>
          </div>
          <!--contents--> 
        </div>
      </article>
    </div>
    <!--container--> 
  </div>
</div>
<!-- TESTIMONIAL END
========================================================================= --> 

<!-- PORTFOLIO END
========================================================================= --> 
<div class="<?php if ($this->router->fetch_class() == 'home') { echo 'white-color '; } ?>padding-box" <?php if ($this->router->fetch_class() == 'home') { echo 'id="testimonial"'; } ?>>
  <div class="container">
    <div class="row header">     <article class="col-xs-12 textbox text-center">
      <h2 class="black">Some of our most popular carriers…</h2>
      <div class="contents partner-carousel text-center">
        <div>
          <figure><span><img src="https://globetopper.com/images/carriers/digicel.jpg" alt=""></span></figure>
        </div>
        <div>
          <figure><span><img src="https://globetopper.com/images/carriers/att.jpg" alt=""></span></figure>
        </div>
        <div>
          <figure><span><img src="https://globetopper.com/images/carriers/etisalat.jpg" alt=""></span></figure>
        </div>
        <div>
          <figure><span><img src="https://globetopper.com/images/carriers/indicom.jpg" alt=""></span></figure>
        </div>
        <div>
          <figure><span><img src="https://globetopper.com/images/carriers/ufone.jpg" alt=""></span></figure>
        </div>
        <div>
          <figure><span><img src="https://globetopper.com/images/carriers/vodafone.jpg" alt=""></span></figure>
        </div>
         <div>
          <figure><span><img src="https://globetopper.com/images/carriers/airtel_nigeria.jpg" alt=""></span></figure>
        </div>
      </div>
      <!--row--> 
    </div>
    <!--container--> 
  </div>
</div>
<!-- PARTNER END
========================================================================= --> 
<?php $this->load->view('footer'); ?>