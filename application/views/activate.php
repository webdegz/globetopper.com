<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width,initial-scale=1">
<title><?php echo $title; ?></title>
<link href="<?php echo base_url(); ?>assets/css/all-stylesheets.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/png">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- Fav and touch icons -->
</head>
<body class="login" id="page-top">
<!-- LOGIN START
========================================================================= -->
<div class="blue-color full-container text-center">
  <div>
    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 login-box"> <a class="logo" href="<?php echo base_url(); ?>"><img alt="logo" src="http://globetopper.clickableco.com/images/logo.png"></a>
      <aside class="col-xs-12 textbox">
        <h2 class="black">Account Activation</h2>        
        <?php
        if ($this->session->userdata('error')) {
            $this->load->view('alerts', array('type' => 'error', 'message' => $this->session->userdata('error')));
        }
        if ($this->session->userdata('success')) {
            $this->load->view('alerts', array('type' => 'success', 'message' => $this->session->userdata('success')));
        }
        ?>
        <!--textbox--> 
      </aside>
      <div class="copyright-text">Copyright © 2015 Globetopper.Com - All right reserved.</div>
      <!--login-box--> 
    </div>
  </div>
  <!--blue-color--> 
</div>
<!-- /. LOGIN ENDS
    ========================================================================= --> 
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script>
</body>
</html>