<?php
if ($type == 'error') {
?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
<strong>Oh snap!</strong> <?php echo $message; ?> </div>
<?php
$this->session->unset_userdata('error');
}

if ($type == 'success') {
?>
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">×</button>
<strong>Howdy!</strong> <?php echo $message; ?> </div>
<?php
$this->session->unset_userdata('success');
}

if($type == 'required'){
?>
<div class="alert alert-danger" style="text-align: left;">
    <?php
    echo '<ul style="margin-left: 20px;">';
    foreach($message as $msg){
        echo '<li style="padding-bottom: 5px;">'.$msg.'</li>';
    }
    echo '</ul>';
    ?>
</div>
<?php
$this->session->unset_userdata('required');
}

if($type == 'validation_errors'){
    echo '<div class="alert alert-danger" style="text-align: left;">';
        echo '<ul style="margin-left: 20px;">';
            echo validation_errors('<li style="padding-bottom: 5px;">', '</li>');
        echo '</ul>';
    echo '</div>';
}
?>