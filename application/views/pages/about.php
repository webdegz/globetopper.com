<?php $this->load->view('header', $this->data); ?>    
       <div class="content col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
      <h4 class="title">Industry and Company Profile.</h4>            
      
      <p>
International Mobile TopUp or IMTU, enables end users to purchase mobile airtime from mobile operators in other countries, which in turn allows people to send mobile minutes to friends and family abroad to use on their prepaid mobile phones. </br>
The Developing world is a predominantly Prepaid Mobile market as demonstrated by the 2014 GSMA International report detailing a 77% global average of Prepaid connections. As mobile penetration continues to grow, IMTU is set to become a larger and larger piece of the projected $636 Billion global remittances by 2017. 

</br>GlobeTopper was formed in 2015 to offer International Mobile TopUp services and related products to a mainly unbanked population, focusing primarily on the GCC, African, SE Asian and Latin American markets. By continuing to add to its’ network of regional providers and by buying in local currency whenever possible, GlobeTopper aims to grow the portfolio of available carriers and products while maximizing minutes offered. This will result in more minutes received by the IMTU recipient and better overall value for the sender.
</br>
</br>
<h3><i>Send More. </i></h3></p>
      <div class="clearfix"></div>                  
    </div>       

<?php $this->load->view('footer', $this->data); ?>