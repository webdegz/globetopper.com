<?php $this->load->view('header', $this->data); ?>    
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
      <h4 class="title">Change Your Password</h4>
      
      <?php
      if(validation_errors()){
          $this->load->view('alerts', array('type' => 'validation_errors'));
      }
      if ($this->session->userdata('success')) {
          $this->load->view('alerts', array('type' => 'success', 'message' => $this->session->userdata('success')));
      }
      ?>
      
      <form id="add_contact" name="add_contact" method="post">                  
        <input type="password" name="password" class="form-control" placeholder="Enter your new password" value="">        
        <input type="password" name="passconf" class="form-control" placeholder="Enter your new password again" value="">        
        <div class="pull-right">
            <input type="submit" name="update_profile" value="Update Password" class="btn btn-primary">
        </div>
      </form>
      <div class="clearfix"></div>      
    </div>
<?php $this->load->view('footer', $this->data); ?>