</div>
  <!-- end container --> 
</div>
<!-- end section -->
<hr>

<!-- SOCIAL SECTION STARTS
========================================================================= -->
<div class="padding-box social-box-c">
  <div class="container">
    <div class="row header">
      <article class="col-xs-12 social-icons text-center">
        <h2 class="white">Join Our Satisfied Users!</h2>
        <ul class="list-inline">
          <li><a href="https://www.facebook.com/globetopper"><span class="fa fa-facebook"></span>facebook</a></li>
          <li><a href="#"><span class="fa fa-twitter"></span>Twitter</a></li>
       
       
          <li><a href="#"><span class="fa fa-linkedin"></span>Linkedin</a></li>
          <li><a href="#"><span class="fa fa-instagram"></span>Instagram</a></li>
        </ul>
        <!--textbox--> 
      </article>
      <!--row--> 
    </div>
    <!--container--> 
  </div>
</div>
<!-- SOCIAL SECTION END
========================================================================= --> 
<!-- FOOTER STARTS
========================================================================= -->
<footer id="footer">
  <div class="container">
    <div class="row header">
     <aside class="col-sm-6 col-xs-12">
        <div class=" col-sm-6 col-xs-12 sitemap">
          <h5 class="white">My Account</h5>
          <ul class="list-group">
                <li><a href="<?php echo base_url().'dashboard/'; ?>">Overview</a></li>
                <li><a href="<?php echo base_url().'history/purchase/'; ?>">TopUp History</a></li>
                <li><a href="<?php echo base_url().'contacts/saved_numbers/'; ?>">Contacts</a></li>

          </ul>
         
        </div>
        
         <div class=" col-sm-6 col-xs-12 sitemap">
          <h5 class="white">TopUp</h5>
          <ul class="list-group">
               <li><a href="<?php echo base_url().'topup/'; ?>">Send TopUp</a></li>
                   <li><a href="#"> Countries</a></li>
                       <li><a href="<?php echo base_url().'dashboard/'; ?>">Promotions</a></li>
                           <li><a href="#">Mobile apps</a></li>
          </ul>
        </div>
        
        
        
      </aside>
       <aside class="col-sm-6 col-xs-12">
        <div class=" col-sm-6 col-xs-12 sitemap">
           <h5 class="white">Help</h5>
          <ul class="list-group">
              <li><a href="https://www.globetopper.com/helpdesk/">Support Center</a></li>
              <li><a href="#">Create a ticket</a></li>
            
            
          </ul>
          <h5 class="white">Affiliates</h5>
          <ul class="list-group">
            <li><a>Affiliate Program</a></li>
            <li><a href="">Signup</a></li>
          </ul>
         
        </div>
        
         <div class=" col-sm-6 col-xs-12 sitemap">
          <h5 class="white">Company</h5>
          <ul class="list-group">
            <li><a href="<?php echo base_url().'about/'; ?>">About GlobeTopper</a></li>
            <li><a href="https://www.globetopper.com/helpdesk">Media Center</a></li>
            <li><a href="">Distributors</a></li>
            <li><a href="https://www.globetopper.com/helpdesk/blog">Blog</a></li>
             <li><a href="https://www.globetopper.com/helpdesk/contact-us/">Contact</a></li>
           
          </ul>
         
        </div>
        
        
        
      </aside>
     
      <div class="col-xs-12 copyright-text">
            <p><i>All product names, logos, and brands are property of their respective owners. All company, product and service names used in this website are for identification purposes only. Use of these names, logos, and brands does not imply endorsement.</i></p>
Copyright © 2015 GlobeTopper.com All right reserved.<br />
      <a href="<?php echo base_url().'privacy_policy/'; ?>">  Privacy Policy </a> |  <a href="<?php echo base_url().'terms/'; ?>">  Terms and Conditions </a>
      
       </div>
      <!--row--> 
    </div>
    <a class="dmtop" href="#page-top"></a> 
    <!--container--> 
  </div>
</footer>
<!-- FOOTER END
========================================================================= -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script src="<?php echo base_url(); ?>assets/js/bootstrap/bootstrap.min.js"></script> 
<!--Jquery Easing--> 
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script> 
<!-- Owl Carousel --> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/owl-carousel/js/owl.carousel.js"></script> 
<!-- LAZY EFFECTS Scripts --> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.unveilEffects.js"></script>
<!-- Popup Scripts --> 
<script src="<?php echo base_url(); ?>assets/js/jquery.prettyPhoto.js"></script> 
<!-- Contact Form Scripts --> 
<script src="<?php echo base_url(); ?>assets/js/jquery.jigowatt.js"></script>
<!-- Custom --> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/custom.js"></script>
<?php
if ($this->router->fetch_class() == 'home'){
?>
<!-- Counter Scripts --> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/countdown.js"></script> 
<!-- Slider Revolution 4.x Scripts --> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/rs-plugin/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/rs-plugin/jquery.themepunch.revolution.min.js"></script> 
<!-- Masonary Porfilio Scripts --> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/masonary/masonry3.1.4.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/masonary/masonry.filter.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/masonary/imagesloaded.js"></script> 
<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('.tp-banner').show().revolution(
	{
		dottedOverlay:"none",
		delay:16000,
		startwidth:1170,
		startheight:700,
		hideThumbs:200,
		
		thumbWidth:100,
		thumbHeight:50,
		thumbAmount:5,
		
		navigationType:"bullet",
		navigationArrows:"solo",
		navigationStyle:"preview4",
		
		touchenabled:"on",
		onHoverStop:"on",
		
		swipe_velocity: 0.7,
		swipe_min_touches: 1,
		swipe_max_touches: 1,
		drag_block_vertical: false,
								
		parallax:"mouse",
		parallaxBgFreeze:"on",
		parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
								
		keyboardNavigation:"off",
		
		navigationHAlign:"center",
		navigationVAlign:"bottom",
		navigationHOffset:0,
		navigationVOffset:20,

		soloArrowLeftHalign:"left",
		soloArrowLeftValign:"center",
		soloArrowLeftHOffset:20,
		soloArrowLeftVOffset:0,

		soloArrowRightHalign:"right",
		soloArrowRightValign:"center",
		soloArrowRightHOffset:20,
		soloArrowRightVOffset:0,
				
		shadow:0,
		fullWidth:"off",
		fullScreen:"on",

		spinner:"spinner4",
		
		stopLoop:"off",
		stopAfterLoops:-1,
		stopAtSlide:-1,

		shuffle:"off",
		
		autoHeight:"off",						
		forceFullWidth:"off",						
						
		hideThumbsOnMobile:"off",
		hideNavDelayOnMobile:1500,						
		hideBulletsOnMobile:"off",
		hideArrowsOnMobile:"off",
		hideThumbsUnderResolution:0,
		
		hideSliderAtLimit:0,
		hideCaptionAtLimit:0,
		hideAllCaptionAtLilmit:0,
		startWithSlide:0,
		fullScreenOffsetContainer: "#header"	
	});
});	//ready
</script>    
<?php
}

if ($this->router->fetch_class() == 'home' || $this->router->fetch_class() == 'topup' || $this->router->fetch_class() == 'contacts') {
?>   
    <script src="<?php echo base_url(); ?>assets/intl-tel-input/build/js/intlTelInput.js"></script>
    <script>
      $("#phone_number").intlTelInput({
        //allowExtensions: true,
        //autoFormat: false,
        //autoHideDialCode: false,
        //autoPlaceholder: false,
        //defaultCountry: "auto",
         geoIpLookup: function(callback) {
//           $.get('http://ipinfo.io', function() {}, "jsonp").always(function(resp) {
//             var countryCode = (resp && resp.country) ? resp.country : "";
//             callback(countryCode);
//           });
         },
        //nationalMode: false,
        //numberType: "MOBILE",
        //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        preferredCountries: [],
        utilsScript: "<?php echo base_url(); ?>assets/intl-tel-input/lib/libphonenumber/build/utils.js"
      });
      <?php
      if($this->input->get('phone_number')){
          ?>
              jQuery(document).ready(function(){
                jQuery('#numberInfo').click();
              });              
          <?php    
      }
      ?>
    </script>
<?php
}
?>    
</body>
</html>