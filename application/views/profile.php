<?php $this->load->view('header', $this->data); ?>    
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
      <h4 class="title">Profile Detail</h4>
      
      <?php
      if(validation_errors()){
          $this->load->view('alerts', array('type' => 'validation_errors'));
      }
      if ($this->session->userdata('success')) {
          $this->load->view('alerts', array('type' => 'success', 'message' => $this->session->userdata('success')));
      }
      ?>
      
      <form id="add_contact" name="add_contact" method="post">          
        <input type="text" name="firstname" class="form-control" placeholder="Firstname" value="<?php echo $user_detail->firstname; ?>">
        <input type="text" name="lastname" class="form-control" placeholder="Lastname" value="<?php echo $user_detail->lastname; ?>">
        <input type="text" name="dob" class="form-control" placeholder="Date of Birth" value="<?php echo $user_detail->dob; ?>">
        <input type="text" name="city" class="form-control" placeholder="City" value="<?php echo $user_detail->city; ?>">
        <input type="text" name="state" class="form-control" placeholder="State" value="<?php echo $user_detail->state; ?>">       
        <select name="country" class="form-control">
            <option value="">Select Country</option>
            <?php
            foreach($countries->records as $country){
                $selected=NULL;
                if($user_detail->country == $country->country_code){
                    $selected='selected="selected"';
                }
                echo '<option '.$selected.' value="'.$country->country_code.'">'.$country->country_name.'</option>';
            }
            ?>
        </select>        
        <input type="text" name="instagram" class="form-control" placeholder="Instagram" value="<?php echo $user_detail->instagram; ?>">
        <input type="text" name="twitter" class="form-control" placeholder="Twitter" value="<?php echo $user_detail->twitter; ?>">
        <input type="text" name="linkedin" class="form-control" placeholder="Linkedin" value="<?php echo $user_detail->linkedin; ?>">
        <div class="pull-right">
            <input type="submit" name="update_profile" value="Update Profile" class="btn btn-primary">
        </div>
      </form>
      <div class="clearfix"></div>      
    </div>
<?php $this->load->view('footer', $this->data); ?>