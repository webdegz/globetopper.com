<?php $this->load->view('header', $this->data); ?>    
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
        <a class="dmbutton btn btn-primary" href="<?php echo base_url().'contacts/add_contact/'; ?>" style="float: right;">Add Contact</a>
      <h4 class="title">Saved Numbers</h4>
      <table class="table table-striped" data-effect="fade">
        <thead>
          <tr>
            <th>#</th>
            <th class="blue">Contact Name</th>
            <th class="blue">Phone Number</th>
            <th class="blue">Date</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if($contacts){
            $x=1;
            foreach((array)$contacts as $contact){
            ?>
            <tr>
              <td><?php echo $x; ?></td>
              <td><?php echo $contact->contact_name; ?></td>
              <td><?php echo $contact->contact_phone; ?></td>
              <td><?php echo $contact->date; ?></td>
            </tr>          
            <?php 
            $x++;
            }
          }else{
            ?>
            <tr>
                <td colspan="4">No contact found</td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
      <div class="clearfix"></div>      
    </div>
<?php $this->load->view('footer', $this->data); ?>