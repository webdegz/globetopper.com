<?php $this->load->view('header', $this->data); ?>    
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
      <h4 class="title">Add Contact</h4>
      
      <?php
      if(validation_errors()){
          $this->load->view('alerts', array('type' => 'validation_errors'));
      }
      if ($this->session->userdata('success')) {
          $this->load->view('alerts', array('type' => 'success', 'message' => $this->session->userdata('success')));
      }
      ?>
      
      <form id="add_contact" name="add_contact" method="post">
        <input type="text" name="contact_name" class="form-control" placeholder="My contact's name is">
        <input type="text" id="phone_number" name="contact_phone" value="93" class="form-control" placeholder="My contact's phone number is">       
        <div class="pull-right">
            <input type="submit" name="save_contact" value="Save Contact" class="btn btn-primary">
        </div>
      </form>
      <div class="clearfix"></div>      
    </div>
<?php $this->load->view('footer', $this->data); ?>