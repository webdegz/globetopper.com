<!doctype html>
<html>
    <head>
        <script>
            var base_url = "<?php echo base_url(); ?>";
        </script>    
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title><?php echo $title; ?></title>
        <link href="<?php echo base_url(); ?>assets/css/all-stylesheets.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/intl-tel-input/build/css/intlTelInput.css">
        <?php
        if ($this->router->fetch_class() == 'home') {
        ?>
            <link href="<?php echo base_url(); ?>assets/css/rs-slider/settings.css" rel="stylesheet" type="text/css">
            <link href="<?php echo base_url(); ?>assets/css/rs-slider/extralayers.css" rel="stylesheet" type="text/css">
            <?php
        }
        ?>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.png" type="image/png">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Fav and touch icons -->
    </head>
    <body id="page-top" <?php if ($this->router->fetch_class() == 'home') {
            echo 'class="home-page"';
        } ?>>
        <div id="loader"></div>
         <?php
            if ($this->router->fetch_class() == 'home') {
                ?>  
        <div id="section-home">
         <?php
           }else{
                ?> 
                 <div id="section-home2">
                 <?php } ?>
            <header id="header"> 
                <!-- NAVIGATION STARTS
            ========================================================================= -->
                <nav id="navigation">
                    <div class="navbar navbar-inverse" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                            <!--  Logo Starts --> 
                            <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="https://globetopper.com/images/logo.png" alt=""></a> 
                            <!-- Logo Ends --> 
                        </div>
                        <div class="collapse navbar-collapse pull-right">
                            <ul class="nav navbar-nav">
                                <li><a href="<?php echo base_url(); ?>"><span class="fa fa-home"></span></a></li>
                                <li><a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">TopUp </a>
                                    <ul class="dropdown-menu">
                                      <li><a href="<?php echo base_url().'topup'; ?>">SEND TOPUP</a></li>
                                      <li><a href="<?php echo base_url().'dashboard'; ?>">PROMOTIONS</a></li>
                                      <li><a href="#">COUNTRIES</a></li>
                                    </ul>
                                </li>    
                                <li><a href="https://www.globetopper.com/helpdesk/">Support</a></li>                       
                            </ul>
                            <ul class="nav navbar-nav nav_top_rt">
                                <?php
                                if($this->user->is_user_login()){
                                    echo '<li><a>';
                                    if($user_detail->firstname){
                                        echo "Hello, ".$user_detail->firstname;
                                    }else{
                                        echo "Hello, ".$user_detail->username;
                                    }
                                    echo '</a></li>';
                                ?>
                                <li><a href="<?php echo base_url().'dashboard/'; ?>">Dashboard</a></li>
                                <li><a href="<?php echo base_url() . 'logout/'; ?>">Logout</a></li>
                                <?php
                                }else{
                                ?>
                                <li><a href="<?php echo base_url().'login/'; ?>">Login</a></li>
                                <li><a href="<?php echo base_url() . 'register/'; ?>">Register</a></li>
                                <?php
                                }
                                ?>                                            
                            </ul>
                        </div>
                        <!--/.nav-collapse --> 
                    </div>
                    <div class="navspacer"></div>
                </nav>
                <!-- /. NAVIGATION ENDS
            ========================================================================= --> 
            </header>

            <?php
            if ($this->router->fetch_class() == 'home') {
                ?>    
                <!-- SLIDER STARTS
                    ========================================================================= -->
                <div id="slider">
                    <div class="tp-banner-container">
                        <div>
                            <ul>
                                <!-- SLIDE  -->
                                <li> 

                                    <!-- LAYERS --> 

                                    <!-- LAYER NR. 1 -->
                                    <h1 class="tp-caption lft customout rs-parallaxlevel-0" style="text-align: center; width: 80%; margin-left: auto; margin-right: auto; color: #FFF; margin-top:  100px; font-size: 50px;">The New Face of Mobile TopUp.</h1>
                                    
                                    <h3 class="tp-caption lft customout rs-parallaxlevel-0" style="text-align: center; width: 80%; margin-left: auto; margin-right: auto; color: #FFF; font-size: 20px;"><i>Send More.</i></h3>
                                    
                                    <!-- LAYER NR. 2 -->
                                    <div class="home_topup tp-caption tp-resizeme customout rs-parallaxlevel-0" style="margin-top: 30px; width: 47%; margin-left: auto; margin-right: auto; text-align: left;">
                                        <form method="get" action="<?php echo base_url().'topup/'; ?>">
                                            <input id="phone_number" name="phone_number" type="tel" value="Please enter the number here" required="required" style="width: 100%; height: 62px;">
                                            <input type="submit" value="TopUp!" style="background: #477AB9; font-weight: 900;font-size: 18px; color: #FFF; border: 1px solid #477AB9; border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px; height: 62px; width: 20%;">
                                        </form>    
                                    </div>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /. SLIDER ENDS
                  ========================================================================= --> 
                <?php
            } else {
                ?>
                <!-- BANNER STARTS
                  ========================================================================= -->
                <aside class="col-xs-12 subbanner">
                    <h1><?php echo $heading; ?></h1>
                    <p><?php echo $heading_slug; ?></p>
                </aside>
                <div class="clearfix"></div>
                <!-- /. BANNER ENDS
                  ========================================================================= --> 
                <?php
            }
            ?>
            <!--section-home--> 
        </div>

        <?php
        if ($this->router->fetch_class() != 'home') {
            ?>
            <div class="padding-box">
                <div class="container clearfix">
                    <?php
                    if(isset($user_detail) && $this->router->fetch_class() != 'promotion' && $this->router->fetch_class() != 'countries' && $this->router->fetch_class() != 'about' && $this->router->fetch_class() != 'contact' && $this->router->fetch_class() != 'terms' && $this->router->fetch_class() != 'privacy_policy'){
                    ?>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <ul class="nav nav-tabs nav-stacked">
                            <li><a href="<?php echo base_url() . 'dashboard/'; ?>">Account Overview</a></li>
                            <li><a href="<?php echo base_url() . 'topup'; ?>">Send TopUp</a></li>
                            <li><a href="<?php echo base_url() . 'history/purchase'; ?>">TopUp History</a></li>
                            <li><a href="<?php echo base_url() . 'contacts/saved_numbers'; ?>">Saved Contacts</a></li>
                            <li><a href="<?php echo base_url() . 'profile'; ?>">My Profile</a></li>        
                            <li><a href="<?php echo base_url() . 'profile/password'; ?>">Change Password</a></li>        
                        </ul>
                    </div>                    
                    <!-- end sidebar -->
                    <?php
                    }
                }
                ?>