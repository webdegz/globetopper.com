<?php $this->load->view('header', $this->data); ?>    
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 text-center">
      <h4 class="title text-left">Account Overview</h4>
      <aside class="col-xs-12 col-sm-6">
          
        <div class="col-sm-12" style="background: #112052; border: 1px solid #112052; border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; height: 130px;">
            <div style="color: #FFF; font-size: 24px; font-weight: normal; margin-bottom: 30px; padding-top: 30px;">TopUps made</div>
            <div style="color: #FFF; font-size: 36px; font-weight: bold;"><?php echo $purchasesMade; ?></div>
        </div>
          
        <div class="col-sm-12 plan1">
          <h4>Recent TopUps</h4>          
          <ul>
            <?php
            if($purchases){
                if(count($purchases) > 0){
                    foreach((array)$purchases as $purchase){
                        ?>
                        <li style="text-align: left; float: left; width: 100%; margin-bottom: 10px;">
                            <div style="float: left; font-weight: bold; font-size: 18px;">
                                +<?php echo $purchase->phoneNumbner; ?>
                            </div>
                            <div style="float: right;">
                                <div class="pakage_price"><span class="blue">USD <?php echo $purchase->amount; ?></span></div>                                
                            </div>
                        </li>
                        <?php                        
                    }
                }else{
                    echo '<li>No TopUp history found</li>';
                }
            }else{
                echo '<li>No TopUp history found</li>';
            }
            ?>                        
          </ul>
          <button aria-expanded="false" class="btn btn-primary btn-lg" type="button" onclick="window.location='<?php echo base_url().'history/purchase/'; ?>'">View TopUp history</button>
        </div>
      </aside>
      <aside class="col-xs-12 col-sm-6">
        
        <div class="col-sm-12" style="background: #112052; border: 1px solid #112052; border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; height: 130px;">
            <div style="color: #FFF; font-size: 24px; font-weight: normal; margin-bottom: 30px; padding-top: 30px;">Saved Contacts</div>
            <div style="color: #FFF; font-size: 36px; font-weight: bold;"><?php echo $savedContacts; ?></div>
        </div>
          
        <div class="col-sm-12 plan1">
          <h4>Favorite TopUp</h4>
          <ul>
            <?php
            if($contacts){
                if(count($contacts) > 0){
                    foreach((array)$contacts as $contact){
                        ?>
                        <li style="text-align: left; float: left; width: 100%; margin-bottom: 10px;">
                            <div style="float: left; font-weight: bold; font-size: 18px;">
                                +<?php echo $contact->contact_phone; ?>
                            </div>
                            <div style="float: right;">
                                <div class="pakage_price"><span class="blue"><a class="btn btn-sm btn-primary" href="<?php echo base_url().'topup/?phone_number='.$contact->contact_phone; ?>; ?>">Send TopUp</a></span></div>                              
                            </div>
                        </li>
                        <?php                        
                    }
                }else{
                    echo '<li>No contact found</li>';
                }
            }else{
                echo '<li>No contact found</li>';
            }
            ?>                        
          </ul>
          <button aria-expanded="false" class="btn btn-primary btn-lg" type="button" onclick="window.location='<?php echo base_url().'contacts/saved_numbers'; ?>'">View Favorite TopUps</button>
        </div>
      </aside>
      <div class="clearfix"></div>
      
      <h4 class="title text-left">Promotions</h4>
      <aside class="col-xs-12 col-sm-12">
        <div class="col-sm-12 plan1">
          <h3>Promo</h3>          
          <ul>
            <li>No promotion found in your region, check back soon!</li>            
          </ul>
          <button aria-expanded="false" class="btn btn-block btn-danger" type="button">View Promotions</button>
        </div>
      </aside>
    </div>
<?php $this->load->view('footer', $this->data); ?>