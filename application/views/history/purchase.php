<?php $this->load->view('header', $this->data); ?>    
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">        
      <h4 class="title">Your TopUps</h4>
      <table class="table table-striped" data-effect="fade">
        <thead>
          <tr>
            <th>#</th>
            <th class="blue">Transaction ID</th>            
            <th class="blue">Phone Number</th>            
            <th class="blue">Amount</th>
            <th class="blue">Date</th>
            <th class="blue">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if($purchases){
            $x=1;
            foreach((array)$purchases as $purchase){
            ?>
            <tr>
              <td><?php echo $x; ?></td>
              <td><?php echo $purchase->TRANSACTIONID; ?></td>
              <td><?php echo $purchase->phoneNumbner; ?></td>
              <td>$<?php echo $purchase->amount; ?></td>
              <td><?php echo $purchase->date; ?></td>
              <td><a href="<?php echo base_url().'history/receipt/'.$purchase->idTransaction; ?>">View receipt</a></td>
            </tr>          
            <?php 
            $x++;
            }
          }else{
            ?>
            <tr>
                <td colspan="4">No TopUp history found</td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
      <div class="clearfix"></div>      
    </div>
<?php $this->load->view('footer', $this->data); ?>