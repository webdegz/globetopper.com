<?php
$this->load->view('header', $this->data);
if(isset($user_detail)){
    echo '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">';
}else{
    echo '<div class="content col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">';
}
?>    
    <h4 class="title">Who do you want to TopUp?</h4>

    <?php
    if (validation_errors()) {
        $this->load->view('alerts', array('type' => 'validation_errors'));
    }
    if ($this->session->userdata('success')) {
        $this->load->view('alerts', array('type' => 'success', 'message' => $this->session->userdata('success')));
    }
    ?>

    <form id="topup" name="topup" method="post" action="<?php echo base_url() . 'payment/'; ?>">                
        <div id="phone_number_cont">
            <label for="phone_number">Phone Number</label>
            <br>
            <input type="text" style="height: 50px;" name="phone_number" id="phone_number" class="form-control" placeholder="Phone Number" value="<?php if ($this->input->get('phone_number')) {
        echo $this->input->get('phone_number');
    } else {
        echo 93;
    } ?>">  
            <div class="form_error" style="display: none;"></div>
        </div>
        <div class="pull-right" id="get_data">
            <br>
            <input type="button" id="numberInfo" value="Go!" class="btn btn-primary">                                    
        </div>

        <div id="products_cont"></div>                                            
        
        <div id="amounts_cont" style="display:none;"></div>

        <div class="loader" id="loader_topup" style="text-align: center; margin-top: 20px; display: none;"><img src="http://chiefbinaryoptions.com/img/spinner-blue.gif" width="128" height="128"></div>

        <div class="pull-right">
            <?php
            //echo '<input type="button" id="reset" value="Reset" class="btn btn-primary">';  
            if (isset($user_detail)) {
                echo '<div id="action_cont" style="display: none;">';
                echo '<input type="submit" name="do_payment" value="Proceed to Payment" class="btn btn-primary" style="display: none; float: left; margin-right: 10px;" disabled="disabled" id="topup_submit">';
                echo '</div>';
            } else {
                echo '<div id="action_cont" style="display: none;">';
                echo '<input type="button" id="login_button" value="Login" class="btn btn-primary" onclick="topupLogin()" style="float: left; margin-right: 10px;">';
                echo '<input type="button" id="register_button" value="Register" class="btn btn-primary" onclick="topupRegister()" style="float: left;">';
                echo '</div>';
            }
            ?>                        
        </div>    
    </form>
    <div class="clearfix"></div>      

<?php $this->load->view('topup/information'); ?>
</div>       

<?php $this->load->view('footer', $this->data); ?>