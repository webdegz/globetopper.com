<label for="amount">Select Amount</label>
<table style="width: 100%" cellpadding="0" cellspacing="0">
    <tr>
        <?php
        $x=1;
        foreach($Amounts as $Amount){            
            echo '<td class="amount" data-amount="'.$Amount['Amount'].'" style="width: 33%; padding-bottom: 10px;">'
                    . '<table style="width: 90%; border: 1px solid #DBDBDB; height: 120px;">'
                        . '<tr>'
                            . '<td style="text-align: center; font-weight: bold; font-size: 22px; width: 100%; background: #FFFFFF;">USD '.$Amount['Amount'].'</td>'
                        . '</tr>'
                        . '<tr>'
                            . '<td style="text-align: center; font-weight: bold; font-size: 22px; color: #3C763D; width: 100%; background: #DBDBDB;">'.$Amount['Code'].' '.$Amount['converstionRate'].'</td>'
                        . '</tr>'
                    . '</table>'
                    . '</td>';
            if($x==3){
                echo '</tr><tr>';
                $x=0;
            }
            $x++;
        }
        ?>        
    </tr>
</table>
<select name="amount" class="form-control" id="amount" style="display:none;">
    <option value="">Select Amount</option>
    <?php    
    foreach($Amounts as $Amount){
        $selected="";
        if($this->input->post('amount')){
            if($Amount == $this->input->post('amount')){
                $selected='selected="selected"';
            }
        }
        echo '<option '.$selected.' value="'.$Amount['Amount'].'">'.$Amount['Amount'].'</option>';
    }
    ?>
</select>