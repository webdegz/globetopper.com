<label for="countryName">Country</label>
<input type="text" class="form-control" id="countryName" value="<?php echo $countryName; ?>" disabled="disabled">  
<input type="hidden" class="form-control" name="countryName" value="<?php echo $countryName; ?>">  

<label for="product">Select Operator</label>
<select name="product" class="form-control" id="product">
    <option value="">Select Operator</option>
    <?php
    foreach($Products as $Product){
        $selected="";
        if($this->input->post('product')){
            if($Product->id == $this->input->post('product')){
                $selected='selected="selected"';
            }
        }
        echo '<option '.$selected.' value="'.$Product->id.'">'.$Product->name.'</option>';
    }
    ?>
</select>