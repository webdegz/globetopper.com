<?php $this->load->view('header', $this->data); ?>    
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
      <h4 class="title">Where do you want to send TopUp?</h4>
      
      <?php
      if(validation_errors()){
          $this->load->view('alerts', array('type' => 'validation_errors'));
      }
      if ($this->session->userdata('success')) {
          $this->load->view('alerts', array('type' => 'success', 'message' => $this->session->userdata('success')));
      }
      ?>
      
      <form id="add_contact" name="add_contact" method="post">
        <input type="text" name="phone_number" class="form-control" placeholder="Phone Number (example: +923323340212)">                
        <div class="pull-right">
            <input type="submit" name="next" value="Next Step" class="btn btn-primary">
        </div>
      </form>
      <div class="clearfix"></div>      
      
      <?php $this->load->view('topup/information'); ?>
    </div>       

<?php $this->load->view('footer', $this->data); ?>