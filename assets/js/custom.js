// JavaScript Document
$(document).ready(function() {    
    'use strict';           
    jQuery('#numberInfo').click(function(){        
        jQuery('.form_error').hide();
        var phoneNumber=jQuery('#phone_number').val();        
        if(phoneNumber == ""){
            jQuery('#phone_number').focus();
            jQuery('#phone_number').next('.form_error').show();
            jQuery('#phone_number').next('.form_error').html('<small>Enter Top-up phone number</small>');
        }else{
            jQuery('#numberInfo').hide();
            jQuery('#loader_topup').show();
            
            jQuery('#phone_number').attr('disabled', 'disabled');
            
            var get_product = getUrlVars()["product"];
            if (typeof get_product != 'undefined'){
                jQuery.post(base_url + "ajax/GetProducts", {phoneNumber: phoneNumber, product: get_product}, function(data) {                                                                       
                    if(data == 'failed'){
                        jQuery('#numberInfo').show();                    
                        jQuery('#phone_number').focus();
                        jQuery('#phone_number').parent('.intl-tel-input').next('.form_error').show();
                        jQuery('#phone_number').parent('.intl-tel-input').next('.form_error').html('<small>Invalid Phone Number</small>');
                        jQuery('#loader_topup').hide();
                        jQuery('#phone_number').removeAttr('disabled'); 
                        jQuery('#phone_number_cont').show();
                    }else{
                        jQuery('#loader_topup').hide();                        
                        jQuery('#phone_number').after('<input id="hidden_phoneNumber" type="hidden" name="phone_number" value="'+phoneNumber+'">');
                        jQuery('#products_cont').html(data);
                        jQuery('#reset').show();
                        jQuery('#phone_number_cont').show();
                        
                        var get_phone_number = getUrlVars()["phone_number"];
                        if (typeof get_phone_number != 'undefined'){
                            var res_country_name = jQuery('#countryName').val();                            
                            var res_country_name_lowercase = res_country_name.toLowerCase();
                            jQuery('.country').each(function(i,el){
                                var country_name_text = jQuery(el).children('.country-name').text();                              
                                if (country_name_text.toLowerCase().indexOf(res_country_name_lowercase) >= 0){                                     
                                    jQuery('.country-list').removeClass('active');
                                    jQuery('.country-list').removeClass('highlight');
                                    jQuery(el).addClass('active');
                                    jQuery(el).addClass('highlight');
                                    var country_code = jQuery(el).attr('data-country-code');                                       
                                    jQuery('.selected-flag').children('.iti-flag').attr('class', 'iti-flag '+country_code);
                                }
                            });
                        }
                        
                        var get_amount = getUrlVars()["amount"];
                        if (typeof get_amount != 'undefined'){
                            jQuery('#product').trigger('change');
                        }
                    }
                });                
            }else{
                jQuery.post(base_url + "ajax/GetProducts", {phoneNumber: phoneNumber}, function(data) {                                                    
                    if(data == 'failed'){
                        jQuery('#numberInfo').show();                    
                        jQuery('#phone_number').focus();
                        jQuery('#phone_number').parent('.intl-tel-input').next('.form_error').show();
                        jQuery('#phone_number').parent('.intl-tel-input').next('.form_error').html('<small>Invalid Phone Number</small>');
                        jQuery('#loader_topup').hide();
                        jQuery('#phone_number').removeAttr('disabled'); 
                        jQuery('#phone_number_cont').show();
                    }else{
                        jQuery('#loader_topup').hide();
                        jQuery('#phone_number').attr('disabled', 'disabled');
                        jQuery('#phone_number').after('<input id="hidden_phoneNumber" type="hidden" name="phone_number" value="'+phoneNumber+'">');
                        jQuery('#products_cont').html(data);
                        jQuery('#reset').show();
                        jQuery('#phone_number_cont').show();
                        
                        var get_phone_number = getUrlVars()["phone_number"];
                        if (typeof get_phone_number != 'undefined'){
                            var res_country_name = jQuery('#countryName').val();
                            var res_country_name_lowercase = res_country_name.toLowerCase();
                            jQuery('.country').each(function(i,el){
                                var country_name_text = jQuery(el).children('.country-name').text();                              
                                if (country_name_text.toLowerCase().indexOf(res_country_name_lowercase) >= 0){                                     
                                    jQuery('.country-list').removeClass('active');
                                    jQuery('.country-list').removeClass('highlight');
                                    jQuery(el).addClass('active');
                                    jQuery(el).addClass('highlight');
                                    var country_code = jQuery(el).attr('data-country-code');                                       
                                    jQuery('.selected-flag').children('.iti-flag').attr('class', 'iti-flag '+country_code);
                                }
                            });
                        }
                    }
                });
            }                         
        }
    });        
    
    jQuery(document.body).on('click', '#reset', function(){
        jQuery('#reset').hide();
        jQuery('#products_cont').html("");
        jQuery('#amounts_cont').html("");
        jQuery('#phone_number').removeAttr('disabled');
        jQuery('#phone_number').val("");
        jQuery('#numberInfo').show();
        jQuery('#topup').remove('#hidden_phoneNumber');
    });  
    
    jQuery(document.body).on('change', '#product', function(){
        var product_id = jQuery(this).val();
        if(product != ""){
            jQuery('#loader_topup').show();
            
            var get_amount = getUrlVars()["amount"];
            if (typeof get_amount != 'undefined'){
                var res_country_name = jQuery('#countryName').val();
                jQuery.post(base_url + "ajax/GetAmounts", {product_id: product_id, amount:get_amount, country: res_country_name}, function(data) {                                
                    if(data){                    
                        jQuery('#loader_topup').hide();
                        jQuery('#amounts_cont').show();
                        jQuery('#amounts_cont').html(data);
                        jQuery('#amount').trigger('change');
                    }
                });
            }else{
                var res_country_name = jQuery('#countryName').val();
                jQuery.post(base_url + "ajax/GetAmounts", {product_id: product_id, country: res_country_name}, function(data) {                                
                    if(data){                    
                        jQuery('#loader_topup').hide();
                        jQuery('#amounts_cont').show();
                        jQuery('#amounts_cont').html(data);
                    }
                });
            }
        }
    });   
    
    jQuery(document.body).on('change', '#amount', function(){
        var amount = jQuery(this).val();
        if(amount != ""){
            jQuery('#topup_submit').removeAttr('disabled');
            jQuery('#action_cont').show();
            jQuery('#topup_submit').show();
        }else{
            jQuery('#action_cont').hide();
            jQuery('#topup_submit').hide();
            jQuery('#topup_submit').attr('disabled');
        }
    });   
    
    jQuery(document.body).on('click', '.amount', function(){
        jQuery(this).children('table').css('box-shadow','7px 7px 5px #888888');
        var clickedAmount=jQuery(this).attr('data-amount');
        jQuery('#amount>option:eq('+clickedAmount+')').prop('selected', true);
        jQuery('#topup_submit').removeAttr('disabled');
        jQuery('#action_cont').show();
        jQuery('#topup_submit').show();
    });
    
    jQuery(document.body).on('keyup', '#phone_number', function(){
        var phone_number = jQuery(this).val();
        jQuery('.country').each(function(i,el){
            if(phone_number == jQuery(el).attr('data-dial-code')){                 
                jQuery('.country-list').removeClass('active');
                jQuery('.country-list').removeClass('highlight');
                jQuery(el).addClass('active');
                jQuery(el).addClass('highlight');
                var country_code = jQuery(el).attr('data-country-code');                
                jQuery('.selected-flag').children('.iti-flag').attr('class', 'iti-flag '+country_code);
            }         
        });
    });
    
    jQuery(document.body).on('click', '.country', function(){
        var dial_code = jQuery(this).attr('data-dial-code');
        jQuery('#phone_number').val(dial_code);
    });
    // ANCHOR SCROLL Top
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > 1) {
            jQuery('.dmtop').css({
                bottom: "25px"
            });
        } else {
            jQuery('.dmtop').css({
                bottom: "-100px"
            });
        }
    });
    jQuery('.dmtop').on("click",function() {
        jQuery('html, body').animate({
            scrollTop: '0px'
        }, 800);
        return false;
    });

   

    /**********ANCHOR SCROLL SCRIPT STARTS */
    $('a.page-scroll').on("bind", function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 70)
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
    /**********OUR SERVICES STARTS */

    //Sort random function

    function random(owlSelector) {
        owlSelector.children().sort(function() {
            return Math.round(Math.random()) - 0.5;
        }).each(function() {
            $(this).appendTo(owlSelector);
        });
    }

    $(".services-carousel").owlCarousel({
        autoPlay: false,
        slideSpeed: 500,
        items: 4,
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [979, 3],
        itemsTablet: [768, 2],
        itemsMobile: [667, 1],
        autoHeight: true,
        pagination: false,
        navigation: true,
        transitionStyle: "fade",
        navigationText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
    });

    /************************************************** PORTFOLIO SECTION */

    if ($("#porfolio-masonry").length > 0) {
        var self = $("#porfolio-masonry");
        self.imagesLoaded(function() {
            self.masonry({
                gutter: 0,
                isAnimated: true,
                itemSelector: ".porfolio_smallbox"
            });
        });
        $(".portfolio-section .filter").on("click",function(e) {
            e.preventDefault();
            $(this).parents('#cartegories').find('li a').removeClass('active')
            $(this).addClass('active');
            var filter = $(this).attr("data-filter");

            self.masonryFilter({
                filter: function() {
                    if (!filter) return true;
                    return $(this).attr("data-filter") == filter;
                }
            });
        });
    }

    /*********************************************** TESTIMONIAL SCRIPT */
    function random(owlSelector) {
        owlSelector.children().sort(function() {
            return Math.round(Math.random()) - 0.5;
        }).each(function() {
            $(this).appendTo(owlSelector);
        });
    }

    $(".testimonial-carousel").owlCarousel({
        autoPlay: true,
        slideSpeed: 500,
        items: 1,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [979, 1],
        itemsTablet: [768, 1],
        itemsMobile: [667, 1],
        autoHeight: true,
        pagination: true,
        navigation: false,
        transitionStyle: "backSlide",
        navigationText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
    });

    /************************************************* PARTNER SCRIPT */
    function random(owlSelector) {
        owlSelector.children().sort(function() {
            return Math.round(Math.random()) - 0.5;
        }).each(function() {
            $(this).appendTo(owlSelector);
        });
    }

    $(".partner-carousel").owlCarousel({
        autoPlay: true,
        slideSpeed: 500,
        items: 6,
        itemsDesktop: [1199, 6],
        itemsDesktopSmall: [979, 3],
        itemsTablet: [768, 3],
        itemsMobile: [667, 1],
        autoHeight: true,
        pagination: false,
        navigation: false,
        transitionStyle: "",
        navigationText: [
            "<i class='fa fa-angle-left'></i>",
            "<i class='fa fa-angle-right'></i>"
        ],
    });

    // Fun Facts
    function count($this) {
        var current = parseInt($this.html(), 10);
        current = current + 1; /* Where 50 is increment */

        $this.html(++current);
        if (current > $this.data('count')) {
            $this.html($this.data('count'));
        } else {
            setTimeout(function() {
                count($this)
            }, 50);
        }
    }

    $(".stat-count").each(function() {
        $(this).data('count', parseInt($(this).html(), 10));
        $(this).html('0');
        count($(this));
    });

    // Search
    var $ctsearch = $('#dmsearch'),
        $ctsearchinput = $ctsearch.find('input.dmsearch-input'),
        $body = $('html,body'),
        openSearch = function() {
            $ctsearch.data('open', true).addClass('dmsearch-open');
            $ctsearchinput.focus();
            return false;
        },
        closeSearch = function() {
            $ctsearch.data('open', false).removeClass('dmsearch-open');
        };

    $ctsearchinput.on('click', function(e) {
        e.stopPropagation();
        $ctsearch.data('open', true);
    });

    $ctsearch.on('click', function(e) {
        e.stopPropagation();
        if (!$ctsearch.data('open')) {

            openSearch();

            $body.off('click').on('click', function(e) {
                closeSearch();
            });

        } else {
            if ($ctsearchinput.val() === '') {
                closeSearch();
                return false;
            }
        }
    });

    // Accordion Toggle
    var iconOpen = 'fa-minus',
        iconClose = 'fa-plus';

    $(document).on('show.bs.collapse hide.bs.collapse', '.accordion', function(e) {
        var $target = $(e.target)
        $target.siblings('.accordion-heading')
            .find('i').toggleClass(iconOpen + ' ' + iconClose);
        if (e.type == 'show')
            $target.prev('.accordion-heading').find('.accordion-toggle').addClass('active');
        if (e.type == 'hide')
            $(this).find('.accordion-toggle').not($target).removeClass('active');
    });


    // tooltip demo
    $("[data-toggle=tooltip]").tooltip();

    // popover demo
    $("[data-toggle=popover]")
        .popover()
});

$(window).load(function() {
    "use strict";

    /* Loading Script */
    $('#loader').fadeOut("slow");

// prettyPhoto
	jQuery('a[data-gal]').each(function() {
		jQuery(this).attr('rel', jQuery(this).data('gal'));
	});  	
	jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
		animationSpeed:'slow',
		theme:'light_square',
		slideshow:false,
		overlay_gallery: false,
		social_tools:false,
		deeplinking:false
	});
});

function topupLogin(){
    var phone_number = jQuery('#phone_number').val();
    var product_id = jQuery('#product').val();
    var amount = jQuery('#amount').val();
    window.location=base_url+'login/?action=redirect&controler=topup&phone_number='+phone_number+'&product='+product_id+'&amount='+amount;
}

function topupRegister(){
    var phone_number = jQuery('#phone_number').val();
    var product_id = jQuery('#product').val();
    var amount = jQuery('#amount').val();
    window.location=base_url+'register/?action=redirect&controler=topup&phone_number='+phone_number+'&product='+product_id+'&amount='+amount;
}

// Read a page's GET URL variables and return them as an associative array.
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}